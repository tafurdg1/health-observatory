<aside class="c-aside">

  <figure class="c-aside__figure">
    <img class="c-aside__img" src="<?php echo BASE_URL . $PATH_IMG . 'logo.png' ?>" alt="">
  </figure>

  <nav class="c-aside__nav">
    <h2 class="c-aside__title">Menu</h2>
    <div class="c-aside__nav_group">
      <a class="c-aside__link c-aside__link_active" href="">Dashboard</a>
      <a class="c-aside__link" href="">Consultas</a>
      <a class="c-aside__link" href="">Administradores</a>
      <a class="c-aside__link" href="">Pacientes</a>
      <a class="c-aside__link" href="">Mis Consultas</a>
    </div>
  </nav>

  <a class="c-aside__logout" href="">Cerrar sesión</a>

</aside>