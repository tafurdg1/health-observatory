<?php
include_once("../../config/routes.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  include_once("../../includes/metas.php");
  ?>
  <title>Lista pacientes</title>
</head>

<body>
  <main class="o-layout-two">

    <?php
    include_once("../../includes/menu_admin.php");
    ?>

    <section class="o-layout-100">

      <?php
      include_once("../../includes/header_top.php");
      ?>

      <div class="o-canvas-panel">

        <?php
        include_once("../../includes/title_section.php");
        ?>

        <div class="c-wrapper-content">

          <article class="c-card c-card_100">

            <table class="c-table">
              <thead>
                <tr>
                  <th>id</th>
                  <th>Nombre completo</th>
                  <th>Documento</th>
                  <th>Celular</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Nombre con apellido</td>
                  <td>1037659975</td>
                  <td>3232860831</td>
                  <td class="c-table__link">
                    <a href="" class="fa fa-trash"></a>
                    <a href="" class="fa fa-pen"></a>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Nombre con apellido</td>
                  <td>1037659975</td>
                  <td>3232860831</td>
                  <td class="c-table__link">
                    <a href="" class="fa fa-trash"></a>
                    <a href="" class="fa fa-pen"></a>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Nombre con apellido</td>
                  <td>1037659975</td>
                  <td>3232860831</td>
                  <td class="c-table__link">
                    <a href="" class="fa fa-trash"></a>
                    <a href="" class="fa fa-pen"></a>
                  </td>
                </tr>
              </tbody>
            </table>

          </article>
          
          <a href="" class="o-btn">Registrar Paciente</a>
          
        </div>

      </div>

      </div>

    </section>

  </main>

</body>

</html>