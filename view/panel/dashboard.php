<?php
include_once("../../config/routes.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  include_once("../../includes/metas.php");
  ?>
  <title>Dashboard | Nombre usuario</title>
</head>

<body>
  <main class="o-layout-two">

    <?php
    include_once("../../includes/menu_admin.php");
    ?>

    <section class="o-layout-100">

      <?php
      include_once("../../includes/header_top.php");
      ?>

      <div class="o-canvas-panel">

        <?php
        include_once("../../includes/title_section.php");
        ?>

        <div class="c-wrapper-content">

          <article class="c-card">
            <h4 class="c-card__title">Total Administradores</h4>
            <p class="c-card__lead">Doctores registrados en el sistema</p>
            <p class="c-card__number">2</p>
          </article>

          <article class="c-card">
            <h4 class="c-card__title">Total Pacientes</h4>
            <p class="c-card__lead">Pacientes registrados en el sistema</p>
            <p class="c-card__number">2</p>
          </article>

          <article class="c-card">
            <h4 class="c-card__title">Total Consultas</h4>
            <p class="c-card__lead">Consultas realizadas en el sistema</p>
            <p class="c-card__number">2</p>
          </article>

          <article class="c-card">
            <blockquote class="twitter-tweet">
              <p lang="es" dir="ltr"><a href="https://twitter.com/hashtag/ReporteCOVID19?src=hash&amp;ref_src=twsrc%5Etfw">#ReporteCOVID19</a> 31 de mayo:<br><br>15.612 recuperados<br>23.177 nuevos casos<br>492 fallecidos<br><br>Muestras: 51.113<br>PCR: 37.317<br>Antígeno: 13.796<br><br>Total:<br><br>3.169.573 recuperados<br>3.406.456 casos <br>88.774 fallecidos <br>17.009.055 muestras procesadas<br>137.070 casos activos<a href="https://t.co/SiKTpTCQ3W">https://t.co/SiKTpTCQ3W</a> <a href="https://t.co/vCl6EIXD5h">pic.twitter.com/vCl6EIXD5h</a></p>&mdash; MinSaludCol (@MinSaludCol) <a href="https://twitter.com/MinSaludCol/status/1399481952395313152?ref_src=twsrc%5Etfw">May 31, 2021</a>
            </blockquote>
            <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
          </article>

        </div>

      </div>

    </section>

  </main>

</body>

</html>