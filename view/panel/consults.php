<?php
include_once("../../config/routes.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  include_once("../../includes/metas.php");
  ?>
  <title>Consultas</title>
</head>

<body>
  <main class="o-layout-two">

    <?php
    include_once("../../includes/menu_admin.php");
    ?>

    <section class="o-layout-100">

      <?php
      include_once("../../includes/header_top.php");
      ?>

      <div class="o-canvas-panel">

        <?php
        include_once("../../includes/title_section.php");
        ?>

        <div class="c-wrapper-content">

          <article class="c-card c-card_30">
            <h4 class="c-card__title">Filtros</h4>

            <form action="" class="c-form">

              <div class="c-form__box">
                <label class="c-form__label" for="">Buscar usuario</label>
                <input class="c-form__input" type="search" placeholder="103765974">
              </div>

              <div class="c-filter">
                <p class="c-form__label">Por tipo de evento</p>
                <div class="c-form__check">
                  <input type="checkbox">
                  <label class="c-form__label" for="">Evento uno</label>
                </div>
                <div class="c-form__check">
                  <input type="checkbox">
                  <label class="c-form__label" for="">Evento uno</label>
                </div>
                <div class="c-form__check">
                  <input type="checkbox">
                  <label class="c-form__label" for="">Evento uno</label>
                </div>
              </div>

              <button class="o-btn">Aplicar filtros</button>
            </form>
          </article>

          <div class="c-card_68">
            
            <article class="c-card c-card_68">
              <div class="c-card__double">

                <div class="c-card__double-box">
                  <h3 class="c-card__title">Paciente: Andres Tafur</h3>
                  <h4 class="c-card__subtitle">Num documento: 1037659775</h4>
                  <p class="c-card__txt c-card__txt_space">Evento: Odontologia</p>

                  <p class="c-card__txt">Encargado: Andrea Camelo</p>
                  <p class="c-card__txt">Departamento: Cundinamarca Madrid</p>
                  <p class="c-card__txt">Fecha consulta: 28/05/2021</p>
                </div>

                <div class="c-card__double-box c-card__double-box_flex">
                  <p class="c-card__txt">25 años</p>
                  <p class="c-card__txt">Hombre</p>
                  <p class="c-card__txt">Semana: 1</p>
                  <a class="o-btn">Ir al detalle</a>
                </div>

              </div>
            </article>

            <article class="c-card c-card_68">
              <div class="c-card__double">

                <div class="c-card__double-box">
                  <h3 class="c-card__title">Paciente: Andres Tafur</h3>
                  <h4 class="c-card__subtitle">Num documento: 1037659775</h4>
                  <p class="c-card__txt c-card__txt_space">Evento: Odontologia</p>

                  <p class="c-card__txt">Encargado: Andrea Camelo</p>
                  <p class="c-card__txt">Departamento: Cundinamarca Madrid</p>
                  <p class="c-card__txt">Fecha consulta: 28/05/2021</p>
                </div>

                <div class="c-card__double-box c-card__double-box_flex">
                  <p class="c-card__txt">25 años</p>
                  <p class="c-card__txt">Hombre</p>
                  <p class="c-card__txt">Semana: 1</p>
                  <a class="o-btn">Ir al detalle</a>
                </div>

              </div>
            </article>

          </div>

        </div>

      </div>

    </section>

  </main>

</body>

</html>