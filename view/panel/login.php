<?php
include_once("../../config/routes.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  include_once("../../includes/metas.php");
  ?>
  <title>Iniciar Sesión</title>
</head>

<body>

  <?php
  include_once("../../includes/header.php");
  ?>

  <main>

    <div class="c-welcome">
      <figure class="c-welcome__figure">
        <img class="c-welcome__img" src="<?php echo BASE_URL . $PATH_IMG . 'bg_login.jpeg' ?>" alt="">
      </figure>

      <div class="c-welcome__box">

        <h1 class="c-welcome__title">Bienvenido al Sistema</h1>

        <form action="" class="c-form">
          <div class="c-form__box">
            <label class="c-form__label" for="document">Número documento</label>
            <input id="document" class="c-form__input" type="number" placeholder="1037659975">
            <small></small>
          </div>
          <div class="c-form__box">
            <label class="c-form__label" for="pwd">Contraseña</label>
            <input id="pwd" class="c-form__input" type="password" placeholder="****">
            <small></small>
          </div>
          <div class="c-form__action">
            <button class="o-btn">Iniciar sesión</button>
          </div>
        </form>
        
      </div>

    </div>

  </main>

</body>

</html>