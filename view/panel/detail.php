<?php
include_once("../../config/routes.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  include_once("../../includes/metas.php");
  ?>
  <title>Detalle | numero consulta</title>
</head>

<body>
  <main class="o-layout-two">

    <?php
    include_once("../../includes/menu_admin.php");
    ?>

    <section class="o-layout-100">

      <?php
      include_once("../../includes/header_top.php");
      ?>

      <div class="o-canvas-panel">

        <?php
        include_once("../../includes/title_section.php");
        ?>

        <div class="c-wrapper-content">

          <article class="c-card c-card_100">
            <div class="c-card__double">

              <div class="c-card__double-box">
                <h3 class="c-card__title">Paciente: Andres Tafur</h3>
                <h4 class="c-card__subtitle">Num documento: 1037659775</h4>
                <p class="c-card__txt c-card__txt_space">Evento: Odontologia</p>

                <p class="c-card__txt">Encargado: Andrea Camelo</p>
                <p class="c-card__txt">Departamento: Cundinamarca Madrid</p>
                <p class="c-card__txt">Fecha consulta: 28/05/2021</p>
              </div>

              <div class="c-card__double-box c-card__double-box_flex">
                <p class="c-card__txt">25 años</p>
                <p class="c-card__txt">Hombre</p>
                <p class="c-card__txt">Semana: 1</p>
              </div>

            </div>
          </article>

          <article class="c-card c-card_100">
            <h3 class="c-card__title">Datos</h3>
            <div class="c-columns">
              <div class="c-col">
                <p class="c-card__txt">Encargado: Andrea Camelo</p>
                <p class="c-card__txt">Departamento: Cundinamarca Madrid</p>
                <p class="c-card__txt">Fecha consulta: 28/05/2021</p>
              </div>
              <div class="c-col">
                <p class="c-card__txt">Encargado: Andrea Camelo</p>
                <p class="c-card__txt">Departamento: Cundinamarca Madrid</p>
                <p class="c-card__txt">Fecha consulta: 28/05/2021</p>
              </div>
              <div class="c-col">
                <p class="c-card__txt">Encargado: Andrea Camelo</p>
                <p class="c-card__txt">Departamento: Cundinamarca Madrid</p>
                <p class="c-card__txt">Fecha consulta: 28/05/2021</p>
              </div>
            </div>

          </article>

          <article class="c-card c-card_100">
            <h3 class="c-card__title">Locación</h3>
            <div class="c-columns">
              <div class="c-col">
                <p class="c-card__txt">Encargado: Andrea Camelo</p>
                <p class="c-card__txt">Departamento: Cundinamarca Madrid</p>
                <p class="c-card__txt">Fecha consulta: 28/05/2021</p>
              </div>
            </div>
          </article>

          <article class="c-card c-card_100">
            <h3 class="c-card__title">Modificaciones</h3>
            <div class="c-columns">
              <div class="c-col">
                <p class="c-card__txt">Fecha modificación: 28/05/2021</p>
                <p class="c-card__txt">Descripción: </p>
              </div>
            </div>
          </article>

        </div>
        <button class="o-btn">Descargar informe</button>

      </div>

      </div>

    </section>

  </main>

</body>

</html>