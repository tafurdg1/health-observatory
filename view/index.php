<?php
include_once("../config/routes.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  include_once("../includes/metas.php");
  ?>
  <title>Página Principal</title>
</head>

<body>

  <?php
  include_once("../includes/header.php");
  ?>

  <main>

    <section class="c-hero">
      <img class="c-hero__img" src="<?php echo BASE_URL . $PATH_IMG . 'bg_hero.jpeg' ?>" alt="">
      <div class="o-canvas">
        <div class="c-hero__box">
          <div class="c-hero__content">
            <h1 class="c-hero__tile">Welcome to Health Observatory</h1>
            <h2 class="c-hero__subtitle">Los mejores especialistas del país</h2>
            <div class="c-hero__callto">
              <a href="<?php echo BASE_URL . 'view/panel/login.php' ?>" class="o-btn">INGRESAR</a>
              <a class="o-btn o-btn_outline">EVENTUALIDADES</a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="o-canvas">
      <header class="o-header">
        <h2 class="o-subtitle">Brindamos atención en varios departamentos</h2>
      </header>
      <div class="c-department">
        <div class="c-department__card">
          <img src="<?php echo BASE_URL . $PATH_IMG . 'plus.svg' ?>" alt="Icono plus">
          <h3 class="c-department__title">Pediatria</h3>
        </div>
        <div class="c-department__card">
          <img src="<?php echo BASE_URL . $PATH_IMG . 'plus.svg' ?>" alt="Icono plus">
          <h3 class="c-department__title">Pediatria</h3>
        </div>
        <div class="c-department__card">
          <img src="<?php echo BASE_URL . $PATH_IMG . 'plus.svg' ?>" alt="Icono plus">
          <h3 class="c-department__title">Pediatria</h3>
        </div>
        <div class="c-department__card">
          <img src="<?php echo BASE_URL . $PATH_IMG . 'plus.svg' ?>" alt="Icono plus">
          <h3 class="c-department__title">Pediatria</h3>
        </div>
      </div>
    </section>

  </main>

</body>

</html>